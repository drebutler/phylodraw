#! /usr/bin/python
'''Draws phylogenetic trees from spreadsheets
Copyright (C) 2013 Eric Butler'''

'''This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
PhyloDraw makes use of TTFQuery.  The TTFQuery license is as follows:

THIS SOFTWARE IS NOT FAULT TOLERANT AND SHOULD NOT BE USED IN ANY
SITUATION ENDANGERING HUMAN LIFE OR PROPERTY.

TTFQuery License

	Copyright (c) 2003, Michael C. Fletcher and Contributors
	All rights reserved.
	
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions
	are met:
	
		Redistributions of source code must retain the above copyright
		notice, this list of conditions and the following disclaimer.
	
		Redistributions in binary form must reproduce the above
		copyright notice, this list of conditions and the following
		disclaimer in the documentation and/or other materials
		provided with the distribution.
	
		The name of Michael C. Fletcher, or the name of any Contributor,
		may not be used to endorse or promote products derived from this 
		software without specific prior written permission.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
	``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
	LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
	FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
	COPYRIGHT HOLDERS AND CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
	INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
	HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
	STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
	ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
	OF THE POSSIBILITY OF SUCH DAMAGE. '''

import sys,csv
from PIL import Image as PILImage
from PIL import ImageDraw as PILImageDraw
from PIL import ImageTk as PILImageTk
from PIL import ImageFont
from Tkinter import *
import ttfquery.findsystem as findsystem
import ttfquery.describe as describe
import tkFileDialog

font = ImageFont.load_default()
fontsize = 16
path = None
pic = None

BLACK = (0,0,0)
WHITE = (255,255,255)

picwidth = 700
picheight = 700
picrotate = 0
linewidth = 1
minroot = 50
forwardpadding = 5
endpadding = 5

def read():
    '''Reads the spreadsheet into the program'''
    #path = 'phylosheet.csv'
    if path == None:
        pass
    else:
        csvfile = open(path)
        dialect = csv.Sniffer().sniff(csvfile.read())
        csvfile.seek(0)
        reader = csv.reader(csvfile, dialect)
        speciesbreakdown = []
        for row in reader:
            speciesbreakdown.append(row)
        maptoDict(speciesbreakdown)

def maptoDict(speciesbreakdown):
    '''Maps spreadsheet data into descent/ancestor dictionaries'''
    cladesDA = {} # descendents:ancestors
    cladesAD = {} # ancestors:descendents
    taxondict = {} # a list by columns
    orderedlist = []
    columnnames = {}
    order = 0
    for row in speciesbreakdown:
        order+=1
        step = 0
        for species in row:
            if species[0:4] != 'SKIP':
                try:
                    namelen = columnnames[step]
                    if len(species) > len(namelen):
                        columnnames[step] = species
                except KeyError:
                    columnnames[step] = species
            else:
                if step not in columnnames.keys():
                    columnnames[step] = ''
            if displayapp.KEEPORDER.get() == 1 and step == 0:
                orderedlist.append((species+'000',order))
            if step < (len(row)-1): # not at the end of the row
                uniqueID = str(step) # create unique ID numbers in case we reuse some labels
                while len(uniqueID) < 3:
                    uniqueID = '0'+uniqueID
                uniqueID2 = str(step+1)
                while len(uniqueID2) < 3:
                    uniqueID2 = '0'+uniqueID2
                descendentID = species+uniqueID # this taxon is the descendent
                ancestorID = row[step+1]+uniqueID2 # the next taxon in the same row is the ancestor
                cladesDA[descendentID] = ancestorID
                try:
                    cladesAD[ancestorID].append(descendentID)
                except KeyError:
                    cladesAD[ancestorID] = [descendentID]
                try:
                    taxondict[step].append(descendentID)
                except KeyError:
                    taxondict[step] = [descendentID]
                try:
                    taxondict[step+1].append(ancestorID) # so most things are put in twice - sue me
                except KeyError:
                    taxondict[step+1] = [ancestorID]
            step+=1
    maxlength = step
    maxwidth = len(speciesbreakdown)+1
    # removes duplicates in cladesAD and taxondict
    for clade in cladesAD:
        cladesAD[clade] = set(cladesAD[clade])
    for column in taxondict:
        taxondict[column] = set(taxondict[column])
    if displayapp.KEEPORDER.get() == 1:
        coordinateAssign(maxwidth,maxlength,taxondict,cladesAD,cladesDA,orderedlist=orderedlist)
    else:
        arrange(maxwidth,maxlength,taxondict,cladesAD,cladesDA,columnnames=columnnames)

def arrange(maxwidth,maxlength,taxondict,cladesAD,cladesDA,columnnames):
    '''Makes a list of terminal taxa in the order that they need to be in to prevent lines from crossing'''
    columns = taxondict.keys()
    columns.sort()
    lastcolumn = columns[-1]
    base = list(taxondict[lastcolumn])[0] # find base of the tree taxon
    termini = []
    done = False
    ordernum = 1
    notUsed = {0:[base]} #  start at the base of the tree
    while done == False:
        delkeys = []
        for key in notUsed:
            if notUsed[key] == []: # if the key is linked to an empty list put on list to delete
                delkeys.append(key)
        for key in delkeys: # delete keys associated with nothing
            del notUsed[key]
        if len(notUsed) <= 0: # check to see if we've checked every taxon
            done = True
        else:
            priorities = notUsed.keys()
            priorities.sort()
            priority = priorities[0]
            ancestor = notUsed[priority].pop() # picks the most-basal unused taxon to start with
            newpriority = priority+1 # to label the priority of the descendents of this guy
            try:
                descendents = list(cladesAD[ancestor])
                try:
                    for descendent in descendents:
                        notUsed[newpriority].append(descendent)
                except KeyError:
                    notUsed[newpriority] = descendents
            except KeyError: # you've hit a terminal taxon
                termini.append((ancestor,ordernum))
                ordernum+=1
    coordinateAssign(maxwidth,maxlength,taxondict,cladesAD,cladesDA,columnnames=columnnames,termini=termini)

def respace(columnnames,length,maxlength):
    '''Goes through a list of column widths and turns them into
    coordinates for the beginning of each column'''
    global picwidth
    keys = columnnames.keys()
    keys.sort()
    end = keys[-1]
    columnwidths = {}
    for x in columnnames:
        columnwidths[x] = font.getsize(columnnames[x])[0]+forwardpadding+endpadding
    if columnwidths[end] < minroot:
        columnwidths[end] = minroot
    total = 0
    for x in columnwidths:
        total+=columnwidths[x]
    if picwidth < total and displayapp.RESIZE.get() == 1:
		picwidth = total
    remnant = picwidth-total
    indRem = 0
    if remnant > maxlength:
        indRem = remnant/len(columnnames)
    columnstarts = {}
    for column in columnnames:
        q = 0
        for x in range(end,column,-1):
            q+=columnwidths[x]+indRem
        columnstarts[column]=q
    return columnstarts

def coordinateAssign(maxwidth,maxlength,taxondict,cladesAD,cladesDA,columnnames=[],termini=[],orderedlist=[]):
    '''Assigns coordinates to species and creates connecting lines'''
    width = picheight/maxwidth # width of the tree, not the screen
    length = picwidth/maxlength # height between each branch layer
    if displayapp.SPACING.get() == 1:
		columnspacing = respace(columnnames,length,maxlength)
    # assign coordinates to species and clades
    coordsdict = {}
    # make coordinates for a tree with the root to the left
    # first assign coordinates to termini
    namelist = [] # a list of actual taxa, not connectors, for labeling
    if displayapp.KEEPORDER.get() == 0:
        for (taxon,order) in termini:
            if displayapp.SPACING.get() == 1:
                coordsdict[taxon] = (picwidth,width*order,columnspacing[0],width*order)
            else:
                coordsdict[taxon] = (picwidth,width*order,picwidth-length,width*order)
    else:
        for (taxon,order) in orderedlist:
            if displayapp.SPACING.get() == 1:
                coordsdict[taxon] = (picwidth,width*order,columnspacing[0],width*order)
            else:
                coordsdict[taxon] = (picwidth,width*order,picwidth-length,width*order)
    # now handle more basal taxa
    column = 0
    step = 1
    for key in taxondict:
        for taxon in taxondict[column]:
            namelist.append(taxon)
            if column == 0:
                pass
            else:
                minY = picheight
                maxY = 0
                for descendent in cladesAD[taxon]:
                    coords = coordsdict[descendent]
                    if coords[1] < minY:
                        minY = coords[1]
                    if coords[1] > maxY:
                        maxY = coords[1]
                if displayapp.SPACING.get() == 1:
                    coordsdict['connector'+taxon]=(columnspacing[column-1]-(linewidth/2),
                            minY,columnspacing[column-1]-(linewidth/2),maxY)
                else:
                    coordsdict['connector'+taxon]=(picwidth-((length*column)-(linewidth/2)),
                            minY,picwidth-((length*column)-(linewidth/2)),maxY)
                y = (minY+maxY)/2
                if displayapp.SPACING.get() == 1:
                    coordsdict[taxon] = (columnspacing[column-1],y,columnspacing[column],y)
                else:
                    coordsdict[taxon] = (picwidth-(length*column),y,picwidth-(length*(column+1)),y)
            step+=1
        column+=1
    drawAll(coordsdict,namelist,length)

def drawAll(coordsdict,namelist,length):
    '''Draws lines and text'''
    global pic
    pic = PILImage.new('RGB',(picwidth,picheight),WHITE)
    draw = PILImageDraw.Draw(pic)
    for species in coordsdict:
        draw.line(coordsdict[species],fill=BLACK,width=linewidth)
        if species in namelist: # manage names
            name = species[:len(species)-3] # remove ID number
            if name[0:4] == 'SKIP':
                pass
            else:
                if displayapp.SPACING.get() == 1:
                    draw.text((coordsdict[species][2]+forwardpadding,coordsdict[species][1]+(linewidth/2)),name,fill=BLACK,font=font)
                else:
                    textlength = font.getsize(species)[0]
                    draw.text((coordsdict[species][0]-textlength,coordsdict[species][1]+(linewidth/2)),name,fill=BLACK,font=font)
    del draw
    rotatePic()

def rotatePic():
    global pic,TkPic
    '''Rotates and displays the finished image'''
    pic = pic.rotate(picrotate,expand=1)
    TkPic = PILImageTk.PhotoImage(pic)
    displayapp.displayTree()

class DisplayApp():
    def __init__(self,root):
        self.AppParent = root
        self.AppParent.title('PhyloDraw')
        self.MainFrame = Frame(self.AppParent)
        self.MainFrame.pack()
        self.CanvasFrame = Frame(self.MainFrame)
        self.CanvasFrame.pack(side=RIGHT)
        self.WidgetFrame = Frame(self.MainFrame)
        self.WidgetFrame.pack(side=LEFT)
        self.KEEPORDER = IntVar()
        self.KEEPORDER.set(0)
        self.SPACING = IntVar()
        self.SPACING.set(1)
        self.RESIZE = IntVar()
        self.RESIZE.set(1)
        self.ROTATE = IntVar()
        self.ROTATE.set(0)
        self.PATH = StringVar()
        self.PATH.set(path)
        self.createWindow()

    def createWindow(self):
        row=0
        self.OpenLabelLab = Label(self.WidgetFrame,text='Use this spreadsheet:')
        self.OpenLabelLab.grid(row=row,column=0)
        row+=1
        self.OpenLabel = Label(self.WidgetFrame,textvar=self.PATH)
        self.OpenLabel.grid(row=row,column=0,columnspan=2)
        row+=1
        self.OpenButton = Button(self.WidgetFrame,text='Select spreadsheet',command=self.OpenFile)
        self.OpenButton.grid(row=row,column=0,columnspan=2)
        row+=1
        self.keepOrder = Checkbutton(self.WidgetFrame,text='Keep order of names',variable=self.KEEPORDER).grid(row=row,column=0,columnspan=2)
        row+=1
        self.Spacing = Checkbutton(self.WidgetFrame,text='Dynamic spacing',variable=self.SPACING).grid(row=row,column=0,columnspan=2)
        row+=1
        self.XsizeLab = Label(self.WidgetFrame,text='Image width').grid(row=row,column=0)
        self.YsizeLab = Label(self.WidgetFrame,text='Image height').grid(row=row,column=1)
        row+=1
        self.Xsize = Entry(self.WidgetFrame)
        self.Xsize.grid(row=row,column=0)
        self.Xsize.insert(END,picwidth)
        self.Ysize = Entry(self.WidgetFrame)
        self.Ysize.grid(row=row,column=1)
        self.Ysize.insert(END,picheight)
        row+=1
        self.RotateLab = Label(self.WidgetFrame,text='The root of the tree should face:').grid(row=row,column=0,columnspan=2)
        row+=1
        self.rootLeft = Radiobutton(self.WidgetFrame,text='Left',variable=self.ROTATE,value=0).grid(row=row,column=0)
        self.rootRight = Radiobutton(self.WidgetFrame,text='Right',variable=self.ROTATE,value=180).grid(row=row,column=1)
        row+=1
        self.rootUp = Radiobutton(self.WidgetFrame,text='Up',variable=self.ROTATE,value=270).grid(row=row,column=1)
        self.rootDown = Radiobutton(self.WidgetFrame,text='Down',variable=self.ROTATE,value=90).grid(row=row,column=0)
        self.FindFonts()
        row+=1
        self.FontSizeLab = Label(self.WidgetFrame,text='Font size:').grid(row=row,column=0)
        self.FontSize = Entry(self.WidgetFrame)
        row+=1
        self.FontSize.grid(row=row,column=0)
        self.FontSize.insert(END,fontsize)
        row+=1
        self.FontListFrame = Frame(self.WidgetFrame)
        self.FontListFrame.grid(row=row,column=0)
        self.FontNames = Listbox(self.FontListFrame)
        self.FontNames.grid(row=row,column=0)
        for lfont in self.fontnames:
            self.FontNames.insert(END,lfont[0][0])
        self.FontScrollbar = Scrollbar(self.FontListFrame)
        self.FontScrollbar.grid(row=row,column=1,sticky=N+S)
        self.FontNames.config(yscrollcommand=self.FontScrollbar.set)
        self.FontScrollbar.config(command=self.FontNames.yview)
        self.FontCanvas = Canvas(self.WidgetFrame,width=100,height=100)
        self.FontCanvas.grid(row=row,column=1)
        self.FontDrawExample()
        row+=1
        self.FontTest = Button(self.WidgetFrame,text='See Font',command=self.FontDrawExample).grid(row=row,column=0)
        row+=1
        Label(self.WidgetFrame,text='Text padding: before label').grid(row=row,column=0)
        self.frontPad = Entry(self.WidgetFrame)
        self.frontPad.grid(row=row,column=1)
        self.frontPad.insert(END,forwardpadding)
        row+=1
        Label(self.WidgetFrame,text='Text padding: after label').grid(row=row,column=0)
        self.backPad = Entry(self.WidgetFrame)
        self.backPad.grid(row=row,column=1)
        self.backPad.insert(END,endpadding)
        row+=1
        self.LineWidthLab = Label(self.WidgetFrame,text='Line width').grid(row=row,column=0)
        self.LineWidth = Entry(self.WidgetFrame)
        self.LineWidth.grid(row=row,column=1)
        self.LineWidth.insert(END,linewidth)
        row+=1
        self.allowResize = Checkbutton(self.WidgetFrame,text='Allow automatic width resizing',variable=self.RESIZE).grid(row=row,column=0,columnspan=2)
        row+=1
        Label(self.WidgetFrame,text='Minimum root length').grid(row=row,column=0)
        self.rootSize = Entry(self.WidgetFrame)
        self.rootSize.grid(row=row,column=1)
        self.rootSize.insert(END,minroot)
        row+=1
        self.Go = Button(self.WidgetFrame,text='Draw the Tree',command=self.Start).grid(row=row,column=0)
        self.SaveButton = Button(self.WidgetFrame,text='Save image',command=self.SaveFile)
        self.SaveButton.grid(row=row,column=1)
        
    def FindFonts(self):
        self.fontlist = findsystem.findFonts()
        self.fontlist.sort()
        self.fontnames = []
        for font in self.fontlist:
            opened = describe.openFont(font)
            name = describe.shortName(opened)
            self.fontnames.append((name,font))
            
    def FontDrawExample(self):
        self.NewFont()
        fontpic = PILImage.new('RGB',(100,100),WHITE)
        draw = PILImageDraw.Draw(fontpic)
        textsize = font.getsize('Font')
        draw.text((textsize[0]/2,textsize[1]/2),'Font',fill='#000000',font=font)
        del draw
        self.TkFontPic = PILImageTk.PhotoImage(fontpic)
        width = self.TkFontPic.width()
        height = self.TkFontPic.height()
        self.FontCanvas.delete(ALL)
        self.FontCanvas.create_image((width/2,height/2),image=self.TkFontPic)
        
    def NewFont(self):
        global fontsize,font
        try:
            fontsize = int(self.FontSize.get())
        except ValueError:
            pass
        try:
            index = self.FontNames.curselection()[0]
            path = self.fontlist[int(index)]
            font  = ImageFont.truetype(path,fontsize)
        except (ValueError,IndexError):
            pass
            
    def OpenFile(self):
        global path
        path = tkFileDialog.askopenfilename()
        if path[-4:] != '.csv':
            path = 'INVALID'
            self.OpenLabel['fg']='#ff0000'
        else:
            self.OpenLabel['fg']='#000000'
        self.PATH.set(path)
        
    def SaveFile(self):
        savepath = tkFileDialog.asksaveasfilename()
        if pic == None:
            pass
        else:
            if savepath[-4:] != '.png':
                savepath+='.png'
            pic.save(savepath,'PNG')
        
    def Start(self):
        global picrotate,picwidth,picheight,linewidth,minroot
        global forwardpadding,endpadding
        picrotate = self.ROTATE.get()
        if picrotate == 0 or picrotate == 180:
            try:
                picwidth = int(self.Xsize.get())
                picheight = int(self.Ysize.get())
            except ValueError:
                pass
        else:
            try:
                picwidth = int(self.Ysize.get())
                picheight = int(self.Xsize.get())
            except ValueError:
                pass
        try:
            linewidth = int(self.LineWidth.get())
        except ValueError:
            pass
        try:
            minroot = int(self.rootSize.get())
        except ValueError:
            pass
        try:
            forwardpadding = int(self.frontPad.get())
        except ValueError:
            pass
        try:
            endpadding = int(self.backPad.get())
        except ValueError:
            pass
        self.NewFont()
        read()

    def displayTree(self):
        width = TkPic.width()
        height = TkPic.height()
        try:
            self.ImageCanvas.destroy()
        except AttributeError:
            pass
        self.ImageCanvas = Canvas(self.CanvasFrame,width=width,
                                height=height)
        self.ImageCanvas.pack()
        self.ImageCanvas.create_image((width/2,height/2),image=TkPic)

root = Tk()
screen_width = root.winfo_screenwidth()
screen_height = root.winfo_screenheight() 
displayapp = DisplayApp(root)
root.mainloop()
